$(function () {
    getTeams(getSearchData(),"get");


    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getTeams(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getTeams(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getTeams(getSearchData(),"get")
        }
    })
})

//从后台获取数据
function getTeams(data,type) {
    MyAjax("/team",{search:JSON.stringify(data)},type,(res)=>{
        USERS=res.teams
        CURRENT_PAGE=res.currentPage
        $("#pageSize").val(res.pageSize);
        $("#current_page").val(res.currentPage);
        $("#all_page").text(res.count);
        COUNT=res.count
        packaging(res.teams)
        loadLeaderToDiv(res.leaders)
    })
}

function getLeader(code) {
    MyAjax(
        "/getUserByTeam",
        {code:code},
        "get",
        (res)=>{
            loadMemberToDiv(res.leaders)
        }
        )
}

function loadMemberToDiv(data) {
    let htmlStr_add=""
    $.each(data,(i,user)=>{
        htmlStr_add+=`
            <option value="${user.code}" name="${user.name}">${user.name}</option>
        `
    })
    $("#teamLeader").html(htmlStr_add)
}

function loadLeaderToDiv(data) {
    let htmlStr_search="<option value='all'>全部</option>"
    $.each(data,(i,user)=>{
        htmlStr_search+=`
            <option value="${user.code}" name="${user.name}">${user.name}</option>
        `
    })
    $("#search_teamLeader").html(htmlStr_search)
}

//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val(),
        leader:$("#search_teamLeader").val(),
    }
    return search
}

function doSearch() {
    getTeams(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,team)=>{
        htmlStr+=`
                <tr>
                <td>${team.id}</td>
                <td>${team.teamCode}</td>
                <td>${team.teamName}</td>
                <td>${team.teamLeaderCode==null?"暂无":team.teamLeaderCode.name}</td>
                <td>${team.teamLeaderCode==null?"暂无":team.teamLeaderCode.code}</td>
                <td>${team.teamCrew}</td>
                <td>${team.enable==1?'正常':'已解散'}</td>
                <td>${team.updateTime}</td>
                <td>${team.createTime}</td>
                <td>
                <div onclick="switchBtn(2);getLeader('${team.teamCode}');getUserById(${team.id})">修改</div>
                <div onclick="delUser('${team.teamCode}')">删除</div>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}

//获取表单提交的数据
function getUserFormData() {
    let team={
        id:$("#id").val(),
        teamCode:$("#code").val(),
        teamName:$("#teamName").val(),
        teamLeader:{code:$("#teamLeader").val()},
        teamLeaderCode:{code:$("#teamLeader").val()},
        enable:getEnable()
    }
    return team;

}

//删除记录
function delUser(id) {
    if(!confirm("是否删除?"))
        return
    MyAjax("/team/"+id,"","Delete",(res)=>{
        if(res.result==0){
            alert("小组成员未解散,不能删除该小组！")
        }
        else if(res.result>0){
            // console.log("删除成功")
            getTeams(getSearchData(),"get");
        }else {
            alert("删除失败，请重试！")
        }
    })
}

//修改记录
function modify() {
    let team=getUserFormData();
    MyAjax("/team",JSON.stringify(team),"post",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            remove_pointer();
            getTeams(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}

//添加记录
function add() {
    MyAjax("/team",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            getTeams(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}

// //获取爱好
// function getHobby() {
//     let hobyys = $("input:checkbox[name=hobby]");
//     let hobby = "";
//     for (let i = 0; i < hobyys.length; i++) {
//         if (hobyys[i].checked) {
//             hobby += hobyys[i].value + ","
//         }
//     }
//     return hobby;
// }



//重置表单数据为空
function resetFormData() {
    $("#id").val("")
    $("#code").val("")
    $("#teamName").val("")
    $("#teamLeader").val("")
    $("#enable").val("")
}

//设置表单数据
function setFormData(team) {

        $("#id").val(team.id)
        $("#code").val(team.teamCode)
        $("#teamName").val(team.teamName)
        $("#teamLeader").val(team.teamLeaderCode.code)
        if(team.enable=='0')
            $("#enable_no").prop("checked","true")
        else
            $("#enable_yes").prop("checked","true")
}

function getUserById(id) {
    MyAjax("/team/"+id,"","get",(res)=>{
        setFormData(res)
    })
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getTeams(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getTeams(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getTeams(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getTeams(search,"get");
}
