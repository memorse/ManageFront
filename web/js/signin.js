$(function () {
    getSigns({name:"",team:getLoginData().groupCode.teamCode},"get");

})

//从后台获取数据
function getSigns(data,type) {
    MyAjax("/user",{search:JSON.stringify(data)},type,(res)=>{
        USERS=res.signins
        CURRENT_PAGE=res.currentPage
        $("#pageSize").val(res.pageSize);
        $("#current_page").val(res.currentPage);
        $("#all_page").text(res.count);
        COUNT=res.count
        packaging(res.users)
        showLatedTime()
    })
}

//动态显示迟到时间
function showLatedTime() {
    let lateds=$("input:radio[name^='status']")
    $.each(lateds,(i,lated)=>{
        lated.onchange=(e)=>{

            if(e.target.value=='lated'&&e.target.checked){
                $("#latedTime"+e.target.id).show()
            }else if(e.target.value!='lated'&& e.target.checked){
                $("#latedTime"+e.target.id).hide()
            }
        }
    })
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,user)=>{
        htmlStr+=`
                <tr>
                <td>${user.code}</td>
                <td>${user.name}</td>
                <td>
                    <label><input type="radio" checked="checked" id="${user.code}" value="good" name="status${user.id}"  >正常</label>
                    <label><input type="radio" value="lated" id="${user.code}" name="status${user.id}"  >迟到</label>
                    <label><input type="radio" value="vacate" id="${user.code}" name="status${user.id}" >请假</label>
                </td>
                
                <td>
                    <input style="display: none" type="datetime-local" id="latedTime${user.code}"/>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}

//获取表单提交的数据
function getUserFormData() {
    let data=[];
    let table =$("#sign_table")[0]
    $.each(table.rows,(i,rows)=>{
        if(i>0){
            let sign=new Object();
            $.each(rows.cells,(j,cell)=>{

                    if(j==0)
                        sign.userCode={code:cell.innerText}
                    // else if(j==1)
                        // sign.userName=cell.innerText
                    else if(j==2){
                        $.each(cell.children,(m,lable)=>{

                            $.each(lable.children,(n,rad)=>{
                                if(rad.checked){
                                    sign.signStatus=rad.value
                                }

                            })
                        })
                    }else if(j==3){
                        $.each(cell.children,(m,time)=>{
                            sign.latedTime=time.value
                        })

                    }

            })
            data.push(sign)
        }

    })
return data
}

//批量点到
function submitSign() {
    MyListAjax("/sign",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            remove_pointer();
            alert("点到完成！")
        }else if(res.result==-1){
            alert(res.message)
        }else {
            alert("请稍后再试！")
        }
    })
}

function quit() {
    $("#add_div").hide();
}
//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}
//获取签到状态信息
function getStatus() {
    return $('input:radio[name="status"]:checked').val();
}

//重置表单数据为空
function resetFormData() {
        $("#id").val("")
        $("#name").val("")
        $("#latedTime").val("")
        $("#signTime").val("")
        $("#enable").val("")
}

//设置表单数据
function setFormData(sign) {

    $("#id").val(sign.id)

    $("#name").val(sign.userCode.name)

    $("#signTime").val(sign.signTime)

    $("#latedTime").val(sign.latedTime)
        if(sign.signStatus=='good')
            $("#sign_good").prop("checked","true")
        else if(sign.signStatus=='late')
            $("#sign_lated").prop("checked","true")
        else if(sign.signStatus=='vacate')
            $("#sign_vacate").prop("checked","true")
        else if(sign.signStatus=='front')
            $("#sign_front").prop("checked","true")

    if(sign.enable=='0')
            $("#enable_no").prop("checked","true")
        else
            $("#enable_yes").prop("checked","true")
}

function getUserById(id) {
    MyAjax("/sign/"+id,"","get",(res)=>{
        setFormData(res)
    })
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getSigns(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getSigns(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getSigns(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getSigns(search,"get");
}
