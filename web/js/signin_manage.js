$(function () {
    getSigns(getSearchData(),"get");
    //绑定搜索框的enter事件
    $("#key").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getSigns(getSearchData(),"get");
        }
    })

//绑定分页查询的enter事件
    $("#current_page").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            let page = $("#current_page").val();
            let search=getSearchData();
            if(page<=1){
                search.currentPage=1
            }else if(page>=COUNT){
                search.currentPage=COUNT
            }
            getSigns(search,"get");
        }
    })

    //绑定分页显示条数
    $("#pageSize").bind("keydown", function (e) {
        if (e.keyCode == "13") {
            getSigns(getSearchData(),"get")
        }
    })

    $("#check_all").change((e)=>{
        let ids = $("input:checkbox[name=check_id]")
        if(e.target.checked){
            console.log("true")
            for (let i = 0; i < ids.length; i++) {
                ids[i].checked=true
            }

        }else {
            console.log("false")
            for (let i = 0; i < ids.length; i++) {
                ids[i].checked=false
            }
        }
    })
})

//从后台获取数据
function getSigns(data,type) {
    MyAjax("/sign",{search:JSON.stringify(data)},type,(res)=>{
        USERS=res.signin
        CURRENT_PAGE=res.currentPage
        $("#pageSize").val(res.pageSize);
        $("#current_page").val(res.currentPage);
        $("#all_page").text(res.count);
        COUNT=res.count
        packaging(res.signin)
        loadTeamListtoDiv(res.teamList)
    })
}


//获取查询条件
function getSearchData() {
    let search={
        key:$("#key").val(),
        currentPage:$("#current_page").val(),
        pageSize:$("#pageSize").val(),
        status:$('input:radio[name="search_status"]:checked').val(),
        startDate:$("#search_startDate").val(),
        endDate:$("#search_endDate").val(),
        enable:$("#search_enable").val(),
        team:$("#search_team").val()
    }
    return search
}

function doSearch() {
    getSigns(getSearchData(),"get")
}

//加载数据到Html
function packaging(data) {
    let htmlStr="";
    $.each(data,(i,sign)=>{
        htmlStr+=`
                <tr>
                
                <td><input type="checkbox" name="check_id" value="${sign.id}"></td>
                <td>${sign.id}</td>
                <td>${sign.userCode.name}</td>
                <td>${sign.signTime}</td>
                <td>${getSignName(sign.signStatus)}</td>
                <td>${sign.latedTime}</td>
                <td>${sign.enable==1?"正常":"不可用"}</td>
                <td>${sign.updateTime}</td>
                <td>${sign.createTime}</td>
                <td>
                <div onclick="switchBtn(2);getUserById(${sign.id})">修改</div>
                </td>
</tr>
            `
    })
    $("#tbody").html(htmlStr)
}


function loadTeamListtoDiv(data) {
    let htmlStr=`<option value="all">全部</option>`
    $.each(data,(i,team)=>{
        htmlStr+=`
            <option value="${team.teamCode}">${team.teamName}</option>
        `
    })
    $("#search_team").html(htmlStr)
}

//获取表单提交的数据
function getUserFormData() {
    let sign={
        id:$("#id").val(),
        signCode:$("#name").val(),
        signTime:$("#signin_time").val(),
        signStatus:getStatus(),
        latedTime:$("#lated_time").val(),
        enable:getEnable()
    }
    return sign;

}

// 获取多选Id
function getIds() {
   let ids = $("input:checkbox[name=check_id]")
    let data = [];
    for (let i = 0; i < ids.length; i++) {
        if (ids[i].checked) {
            data.push(ids[i].value)
        }
    }
    return data;
}

//删除记录
function deleteSign() {

    let ids=getIds()
    MyAjax("/sign",JSON.stringify(ids),"Delete",(res)=>{
        if(res.result>0){
            // console.log("删除成功")
            $("#check_all")[0].checked=false
            getSigns(getSearchData(),"get");
        }else {
            alert("删除失败,请重试！")
        }
    })
}

//修改记录
function modify() {
    let sign=getUserFormData();
    MyAjax("/sign",JSON.stringify(sign),"post",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            remove_pointer();
            getSigns(getSearchData(),"get");
        }else {
            alert("请稍后再试！")
        }
    })
}

//添加记录
function add() {
    MyAjax("/sign",JSON.stringify(getUserFormData()),"PUT",(res)=>{
        if(res.result>0){
            $("#add_div").hide();
            getSigns(getSearchData(),"get");
            remove_pointer();
        }else {
            alert("请稍后再试！")
        }
    })
}


//=========================================

var USERS;
var CURRENT_PAGE;
var COUNT;

//获取状态信息
function getEnable() {
    return $('input:radio[name="enable"]:checked').val();
}
//获取签到状态信息
function getStatus() {
    return $('input:radio[name="status"]:checked').val();
}

//重置表单数据为空
function resetFormData() {
        $("#id").val("")
        $("#name").val("")
        $("#lated_time").val("")
        $("#signin_time").val("")
        $("#enable").val("")
}

//设置表单数据
function setFormData(sign) {

    $("#id").val(sign.id)

    $("#name").val(sign.userCode.name)

    $("#signin_time").val(sign.signTime)

    $("#lated_time").val(sign.latedTime)

    if(sign.signStatus=='good')
        $("#sign_good").prop("checked","true")
    else if(sign.signStatus=='lated')
        $("#sign_lated").prop("checked","true")
    else if(sign.signStatus=='vacate')
        $("#sign_vacate").prop("checked","true")
    else if(sign.signStatus=='front')
        $("#sign_front").prop("checked","true")

    if(sign.enable=='0')
            $("#enable_no").prop("checked","true")
        else
            $("#enable_yes").prop("checked","true")
}

function getUserById(id) {
    MyAjax("/sign/"+id,"","get",(res)=>{
        setFormData(res)
    })
}

//根据签到状态码获取签到状态的名称
function getSignName(stasus) {
    result=""
    if(stasus=='good')
        result="已到"
    else if(stasus=='lated')
        result="迟到"
    else if(stasus=='vacate')
        result="请假"
    else if(stasus=='front')
        result="早退"

    return result;
}

//隐藏提示标签
function resetTipToNone() {
    $("#confirm_password_tip").css("display","none");
    $("#password_tip").css("display","none");
    $("#nickname_tip").css("display","none");
    $("#name_tip").css("display","none");
    $("#account_tip").css("display","none");
}

//添加遮罩
function add_pointer() {
    $(".find").addClass("setpointer")
    $("table").addClass("setpointer")
}

//移除遮罩
function remove_pointer() {
    $(".find").removeClass("setpointer")
    $("table").removeClass("setpointer")
}

//改变显示  添加<===>修改
function switchBtn(flag) {
    $("#add_div").show();
    add_pointer();
    // getGender();
    if (flag == 1) {
        $("#addbth").show()
        $("#modifybth").hide();
        resetFormData();
    } else {
        $("#addbth").hide()
        $("#modifybth").show();

    }
}

//分页模块
function firstPage() {
    let search = getSearchData()
    search.currentPage=1
    getSigns(search,"get")
}

function lastPage() {
    let search = getSearchData()
    search.currentPage=COUNT
    getSigns(search,"get")
}

function prePage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page <= 1){
        search.currentPage=1
    }else {
        page=Number(page) - Number(1)
        search.currentPage=page
    }
    getSigns(search,"get");
}

function nextPage() {
    let page = $("#current_page").val();
    let search=getSearchData();
    if(page >= COUNT){
        search.currentPage=COUNT
    }else {
        page=Number(page) + Number(1)
        search.currentPage=page
    }
    getSigns(search,"get");
}
